from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField, BooleanField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length

class PublicationForm(FlaskForm):
    title = StringField('Titre', validators=[DataRequired()])
    body = StringField('Description', validators=[DataRequired()])
    author = StringField('Auteur', validators=[DataRequired()])

class LoginForm(FlaskForm):
    username = StringField('Pseudo', validators=[DataRequired(), Length(min=3, max=30)])
    password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=3, max=30)])

class RegisterForm(FlaskForm):
    username = StringField('Pseudo', validators=[DataRequired(), Length(min=3, max=30)])
    password = PasswordField('Mot de passe', validators=[DataRequired(), Length(min=3, max=30)])
    first_name = StringField('Prénom', validators=[DataRequired()])
    last_name = StringField('Nom', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
