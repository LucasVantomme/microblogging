## Projet Microblogging

## Etapes pour utiliser le projet ?

1. `git clone`
2. `pipenv --python 3`
3. `pipenv install`
4. `flask initdb`
5. `flask fakedata`
5. `flask monuser` Afin d'avoir un utilisateur ayant comme identifiant Root et toor
6. `flask run`

## Fait par Lucas VANTOMME