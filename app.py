import requests
from flask import Flask, render_template, redirect, url_for, request, session
from flask_login import LoginManager, current_user, login_user, logout_user, login_required, UserMixin
from models import create_tables, drop_tables, Publication, User
from forms import PublicationForm, LoginForm, RegisterForm

connected = False

app = Flask(__name__)
app.secret_key = 'Blah'
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/post/create', methods=['GET', 'POST', ])
def post_create():
    publication = Publication()
    form = PublicationForm()
    if form.validate_on_submit():
        form.populate_obj(publication)
        publication.save()
        return redirect(url_for('posts'))
    return render_template('post/create.html', form=form)


@app.route('/post/edit/<int:publication_id>', methods=['GET', 'POST', ] )
def post_edit(publication_id):
    post = Publication.get(id=publication_id)
    if request.method == 'POST':
        form = PublicationForm(request.form, obj=post)
        if form.validate():
            form.populate_obj(post)
            post.user = current_user.id
            post.save()
            return redirect(url_for('posts'))
    else:
        form = PublicationForm(obj=post)
    return render_template('post/edit.html', form=form, post=post)


@app.route('/posts')
@login_required
def posts():
    return render_template('posts.html', posts = Publication.select())

@app.route('/username/register', methods=['GET', 'POST', ])
def register():
    user = User()
    form = RegisterForm()
    if form.validate_on_submit():
        form.populate_obj(user)
        user.save()
        return redirect(url_for('index'))
    return render_template('auth/register.html', form=form)

@app.route('/username/login', methods=['GET', 'POST', ])
def login():
    user=User()
    form=LoginForm()
    if request.method== 'POST':
        try:
            user=User.select().where(User.username==request.form['username']).get()
        except:
            return render_template('auth/login.html',form=form, user=user, error=1)
        
            
        if request.form['password']==user.password:
            session['id']=user.id
            session['username']=user.username
            login_user(user)
            connected = True
        else:
            return render_template('auth/login.html',form=form, user=user, error=1)
                
        
        return redirect(url_for('posts'))
        
    else:
        return render_template('auth/login.html',form=form, user=user)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))




@app.cli.command()
def initdb():
    """Create database"""
    create_tables()

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()

@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()
    for user_ex in range(0, 5):
        user = User.create(username=fake.last_name(), password=fake.password(), first_name = fake.first_name(), last_name=fake.last_name(), email = fake.email())
        for publications_ex in range(0, 10):
            Publication.create(title = fake.sentence(), body = fake.text(), author=user)

@app.cli.command()
def monuser():
    from faker import Faker
    fake = Faker()
    user=User.create(username='Root', password='toor',first_name = fake.first_name(), last_name=fake.last_name(), email = fake.email())
    Publication.create(title = fake.sentence(), body = fake.text(),author=user)