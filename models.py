from peewee import *
from flask_login import UserMixin
import datetime

database = SqliteDatabase("data.sqlite3")

class BaseModel(Model):

    class Meta:
        database = database

class User(BaseModel,UserMixin):

    username = CharField(max_length=20)
    password = CharField()
    first_name = CharField(max_length=20)
    last_name = CharField(max_length=20)
    email = CharField(max_length=20)
    created_at = DateTimeField(default=datetime.datetime.now)

class Publication(BaseModel):

    title = CharField(max_length=50)
    body = TextField()
    author = ForeignKeyField(User, backref="publications")

def create_tables():
    with database:
        database.create_tables([Publication, User])


def drop_tables():
    with database:
        database.drop_tables([Publication, User])