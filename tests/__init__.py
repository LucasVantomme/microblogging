from models import database, Publication, User

# Create User and Publication tables
def create_tables():
    with database:
        database.create_tables([User, Publication])

# Delete User and Publication tables
def drop_tables():
    with database:
        database.drop_tables([User, Publication])